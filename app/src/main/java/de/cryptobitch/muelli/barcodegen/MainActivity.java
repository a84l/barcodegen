package de.cryptobitch.muelli.barcodegen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import java.util.EnumMap;
import java.util.Map;
import java.util.Random;

import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import static android.widget.Toast.makeText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        LinearLayout l = new LinearLayout(this);
//        l.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//        l.setOrientation(LinearLayout.VERTICAL);
//
//        setContentView(l);



//
//        //barcode text
//        TextView tv = new TextView(this);
//        tv.setGravity(Gravity.CENTER_HORIZONTAL);
//        tv.setText(barcode_data);
//
//        l.addView(tv);

        TextWatcher tw = new MyTextWatcher();
        assert (tw != null);
        {
            EditText e = (EditText) findViewById(R.id.code);
            assert (e != null);
            if (e == null) {
                makeText(MainActivity.this, "hallo null", Toast.LENGTH_SHORT).show();
            } else {
                e.addTextChangedListener(tw);
                CharSequence cq = e.getText();
                e.setText("");
                // Just to make the change watcher run
                e.setText(cq);
            }
        }


        BarcodeFormat[] formats = BarcodeFormat.values();
        String[] format_names = new String[formats.length];
        for (int i=0; i<formats.length; i++) {
            BarcodeFormat bfmt = formats[i];
            String name = bfmt.toString();
//            Toast.makeText(MainActivity.this, "name " + name, Toast.LENGTH_SHORT).show();
            format_names[i] = name;
        }

        Spinner dropdown = (Spinner)findViewById(R.id.spinner);
        String[] items = new String[]{"1", "2", "three"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, format_names);
        dropdown.setAdapter(adapter);
        dropdown.setSelection(4);
        dropdown.setOnItemSelectedListener(new SelectedListener());
    }

    private class SelectedListener implements AdapterView.OnItemSelectedListener {
        public void onNothingSelected(AdapterView<?> parent) {

        }

            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                String text = "Selected " + position + " with id " + id;
                CharSequence cs = text;

                EditText e = (EditText) findViewById(R.id.code);
                CharSequence cq = e.getText();
                e.setText("");
                // Just to make the change watcher run
                e.setText(cq);


                switch (position) {
                case 0:
                    // Whatever you want to happen when the first item gets selected
                    break;
                case 1:
                    // Whatever you want to happen when the second item gets selected
                    break;
                case 2:
                    // Whatever you want to happen when the thrid item gets selected
                    break;

            }
        }
    }

    private class MyTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Spinner dropdown = (Spinner)findViewById(R.id.spinner);
            int position = dropdown.getSelectedItemPosition();
            if (position < 0 || position >= dropdown.getAdapter().getCount() ) {
                position = 1;
            }
            BarcodeFormat format = BarcodeFormat.values()[position];




            EditText e = (EditText) findViewById(R.id.code);
            String barcode_data = e.getText().toString();

            // barcode image
            Bitmap bitmap = null;
            ImageView iv =  (ImageView) findViewById(R.id.imageView);

            try {

                bitmap = encodeAsBitmap(barcode_data, format, 1600, 1300);
                iv.setImageBitmap(bitmap);

            } catch (WriterException ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }


    /**************************************************************
     * getting from com.google.zxing.client.android.encode.QRCodeEncoder
     *
     * See the sites below
     * http://code.google.com/p/zxing/
     * http://code.google.com/p/zxing/source/browse/trunk/android/src/com/google/zxing/client/android/encode/EncodeActivity.java
     * http://code.google.com/p/zxing/source/browse/trunk/android/src/com/google/zxing/client/android/encode/QRCodeEncoder.java
     */

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

    public void onImageClick (View v) {
        StringBuilder sb = new StringBuilder();
        Random r = new java.util.Random();
        for (int i=0; i<10; i++) {
            sb.append(
                    java.lang.Math.abs(r.nextInt() % 10)
            );
        }

        EditText e = (EditText) findViewById(R.id.code);
        e.setText(sb.toString());
    }

}